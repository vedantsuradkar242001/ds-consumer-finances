import pandas as pd
import plotly.express as px
import wqet_grader
from dash import Input, Output, dcc, html
from IPython.display import VimeoVideo
from jupyter_dash import JupyterDash
from scipy.stats.mstats import trimmed_var
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.metrics import silhouette_score
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler

df = pd.read_csv("./scfp2019excel.csv.gz")
df.head()

inccat_dict = {
	1:"0-20"
	2:"2-39.9"
	3:"40-59.9"
	4:"60-79.9"
	5:"80-89.9"
	6:"90-100"
}
df_inccat = (
	df["INCCAT"]
	.replace(inccat_dict)
	.groupby(df["HUBS"])
	.value_counts(normalize=True)
	.rename("frequency")
	.to_frame()
	.reset_index()
	)
df_inccat

sns.barplot(

x="INCCAT",

y="frequency",

data=df_inccat,

hue "HBUS", order inccat_dict.values()

plt.xlabel("<Your x_Title>") plt.ylabel("<Your y_Title>")

plt.title("<Your Title>");

sns.scatterplot(x=df["DEBT"] / 1e6, y=df["HOUSES"]/1e6, palette="deep") plt.xlabel("Household Debt")

plt.ylabel("Home Value") pit.title("Home Value vs. Household Debs");

mask=(df["HUBS"]) & (df["INCOME"]<500_000)
df_small_biz = df[mask]
df_small_biz.head()

top_ten_trim_var = df_small_biz.apply(trimmed_var, limits=(0.1,0.9)).sort_values().tail(10)
top_ten_trim_var

high_var_cols = top_ten_trim_var.tail().index.to_list()

X = df_small_biz[high_var_cols]

n_clusters = range(2, 13) 
inertia_errors= []

silhouette_scores = []

# Use for Loop

for k in n_clusters:

	model = make_pipeline(StandardScaler(), KMeans(n_clusters=k, random_state=42)) #Train

	model.fit(X)

	#Calculate inertia 
	inertia_errors.append(model.named_steps["kmeans"].inentia_)

	#Calculate silhouette score 
	silhouette_scores.append( silhouette_score (X, model.named_steps["kmeans"].labels_))

print("Inertia:", Inertia errors[:10])

print()

print("Silhouette Scores: ", silhouette_scores[:3])

fig = px.line(x=n_clusters, y=inertia_errors)
fig.show()

fig2 = px.line(x=n_clusters, y=silhouette_scores)
fig2.show()

final_model=make_pipeline(StandardScaler(),KMeans(n_clusters=3,random_state=42))
final_model.fit(X)


pca =PCA(n_components=2, random_state=42)
X_t = pca.fit_transform(X)
X_pca = pd.DataFrame(X_t, columns=["PC1","PC2"])
fig px.scatter(
	data_frame = X_pca,
	x="PC1", y="PC2",
	color =labels.astype(str),
	title="PCA Representation of Clusters")

fig.update_Layout(xaxis_title= "PC1", yaxis_title="PC2") 

fig.show()